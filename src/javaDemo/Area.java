package javaDemo;

public class Area {
	public static void main(String[] args) {
		double rectangleWidth = 5.0;
		double rectangleHeight = 10.0;
		double rectangleArea = rectangleWidth * rectangleHeight;
		System.out.println("Area of Rectangle: " + rectangleArea);

		double squareSide = 5.0;
		double squareArea = squareSide * squareSide;
		System.out.println("Area of Square: " + squareArea);

		double circleRadius = 5.0;
		double circleArea = Math.PI * (circleRadius * circleRadius);
		System.out.println("Area of Circle: " + circleArea);
	}
}
