package javaDemo;

public class Main {
	public static void main(String[] args) {
		Carr myCar1 = new Carr();
		Carr myCar2 = new Carr();
		driveCar(myCar1);
		driveCar(myCar2);
	}

	public static void driveCar(Carr c) {
		c.drive();
		c.stop();
	}
}