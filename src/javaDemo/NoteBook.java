package javaDemo;

public class NoteBook extends Book {
	public void write() {
		System.out.println("Writing in a notebook");
	}

	public void read() {
		System.out.println("Reading from a notebook");
	}

	public void draw() {
		System.out.println("Drawing in a notebook");
	}

	public static void main(String[] args) {
		NoteBook nb = new NoteBook();
		nb.write();
		nb.read();
		nb.draw();
	}
}
