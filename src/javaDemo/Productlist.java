package javaDemo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Productlist {

	public static void main(String[] args) {
		// Using ArrayList
		List<String> productsArray = new ArrayList<>();
		productsArray.add("Pepsi");
		productsArray.add("Coke");
		productsArray.add("Sprite");
		productsArray.add("Fanta");
		productsArray.add("ThumbsUp");

		System.out.println("ArrayList");
		System.out.println("Size of the list: " + productsArray.size());
		System.out.println("Contents of the list: " + productsArray);

		productsArray.remove(1);
		System.out.println("After removing the 2nd item, size of the list: " + productsArray.size());
		System.out.println("Contents of the list: " + productsArray);

		if (productsArray.contains("Coke")) {
			System.out.println("Coke is present in the list.");
		} else {
			System.out.println("Coke is not present in the list.");
		}

		// Using LinkedList
		List<String> productsLinked = new LinkedList<>();
		productsLinked.add("Pepsi");
		productsLinked.add("Coke");
		productsLinked.add("Sprite");
		productsLinked.add("Fanta");
		productsLinked.add("ThumbsUp");

		System.out.println("\nLinkedList");
		System.out.println("Size of the list: " + productsLinked.size());
		System.out.println("Contents of the list: " + productsLinked);

		productsLinked.remove(1);
		System.out.println("After removing the 2nd item, size of the list: " + productsLinked.size());
		System.out.println("Contents of the list: " + productsLinked);

		if (productsLinked.contains("Coke")) {
			System.out.println("Coke is present in the list.");
		} else {
			System.out.println("Coke is not present in the list.");
		}
	}
}
