package javaDemo;

public class Santro extends Car implements BasicCar {
	public void RemoteStart() {
		System.out.println("Remote starting the car");
	}

	public void gearChange() {
		System.out.println("Changing gear");
	}

	public void music() {
		System.out.println("Playing music");
	}

	public static void main(String[] args) {
		Santro mySantro = new Santro();
		mySantro.RemoteStart();
		mySantro.drive();
		mySantro.gearChange();
		mySantro.music();
		mySantro.stop();
	}
}
